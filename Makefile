FIREJAIL-NET_REPOSITORY := $(shell pwd)
FIREJAIL-NET_DIR := /usr/local/bin

firejail-net: install

help:
	@echo ""
	@echo "This Makefile supports the following targets:"
	@echo ""
	@echo " make help - show this text"
	@echo " make requirements - resolve package dependencies for firejail-net"
	@echo " make install - install the firejail-net script to $(FIREJAIL-NET_DIR)"
	@echo " make systemd - create a systemd service for firejail-net"
	@echo " make uninstall - fully uninstall all components of firejail-net"
	@echo ""

all: requirements install systemd

requirements:
	@apt-get update
	@apt-get --assume-yes install dnsmasq-base firejail

install:
	@echo "Installing firejail-net to '$(FIREJAIL-NET_DIR)'"
	@cp $(FIREJAIL-NET_REPOSITORY)/firejail-net $(FIREJAIL-NET_DIR)/
	@chmod 700 /usr/local/bin/firejail-net

systemd:
	@cp $(FIREJAIL-NET_REPOSITORY)/systemd/firejail-net.service /usr/lib/systemd/system/
	@systemctl daemon-reload
	@systemctl enable firejail-net.service > /dev/null 2>&1
	@systemctl start firejail-net.service

uninstall:
	@echo "Uninstalling all files related to firejail-net"
	@systemctl disable firejail-net.service > /dev/null 2>&1
	@systemctl stop firejail-net.service
	@sleep 5
	@rm -f /usr/lib/systemd/system/firejail-net.service
	@systemctl daemon-reload
	@rm -f $(FIREJAIL-NET_DIR)/firejail-net
