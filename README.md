# firejail-net

A Linux bridge interface for Firejail using NAT.

## Functionality:

Install firejail-net, and specify `--net=jailbr0` when creating your sandboxes to enable NAT-based networking. Why would you want to do this? To protect against IP leaks, such as those related to WebRTC, etc.

## Dependencies:

Firejail-net requires the following dependencies, (debian/Ubuntu):

* make
* dnsmasq-base
* firejail, (recommended)

```bash
# apt update && apt install make dnsmasq-base firejail
```

Note: if you have GNU make installed, you can resolve dependencies as follows:

```bash
# make requirements
```

To see the full help, run `make help`.

## Installation:

Install firejail-net:

```bash
# make install
```
## Usage:

Bring-up the network bridge for firejail-net:

```bash
# firejail-net [ -u | --up ]
```

To tear-down the network bridge:

```bash
# firejail-net [ -d | --down ]
```

Display help:

```bash
# firejail-net [ -h | --help ]
```

## systemd Service:

To add a systemd service and start firejail-net automatically on startup:

```bash
# make systemd
```

To install BOTH firejail-net and create the service:

```bash
# make all
```

## Uninstall:

To completely uninstall firejail-net:

```bash
# make uninstall
```

## Packaging:

There are plans to:

* Package firejail-net.
* Use reproducible builds.
* Extend support to other Linux distributions.

## Links:

* For information on [Firejail][0] sandboxes.
* Information on [reproducible-builds][1] can be found here.

[0]: https://firejail.wordpress.com/
[1]: https://reproducible-builds.org/
